# DevOps Mise en place une chaine CI CD



## Getting started

Ce projet, développé par Houssem Ben Khammou, vise à établir une chaîne d'intégration et de déploiement continus (CI/CD) pour une application.

Étapes du projet
Mise en place de la chaîne CI/CD : Développement d'une chaîne CI/CD complète pour une application.

Intégration de Jenkins : Configuration de Jenkins pour automatiser les processus CI/CD.

Build et déploiement Docker : Création et gestion des images Docker pour l'application, avec publication sur Docker Hub.

Tests de qualité de code : Utilisation de SonarQube pour analyser la qualité du code et détecter les bugs et vulnérabilités.

Création du cluster Kubernetes : Mise en place et initialisation d'un cluster Kubernetes avec un nœud master et un nœud worker sur deux machines distinctes via des playbooks Ansible et des connexions SSH.

Déploiement avec Helm : Déploiement de l'application sur le cluster Kubernetes en utilisant des charts Helm, incluant la configuration des pods, services et volumes persistants (PVC et PV).

Supervision et monitoring : Mise en place de la surveillance du cluster avec Prometheus et Grafana pour l'analyse des performances.

Utilisation de conteneurs Docker : Déploiement de Jenkins et SonarQube en tant que conteneurs Docker sur les machines Kubernetes (master et worker).

Sécurisation des communications : Gestion de la communication sécurisée entre les machines via SSH.

Ce document décrit les étapes nécessaires pour mettre en place une chaîne CI/CD efficace et sécurisée en utilisant des outils modernes tels que Jenkins, Docker, SonarQube, Kubernetes, Helm, Prometheus et Grafana.