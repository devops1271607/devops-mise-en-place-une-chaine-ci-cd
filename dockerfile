# Utilisez une image de base Jenkins
FROM jenkins/jenkins:latest

# Définissez l'utilisateur root pour effectuer des opérations privilégiées
USER root

# Installez les dépendances nécessaires (si nécessaire)
RUN apt-get update && apt-get install -y curl

# Exécutez les commandes supplémentaires pour configurer Jenkins ou installer des plugins (si nécessaire)
# Exemple d'installation d'un plugin Jenkins : RUN /usr/local/bin/install-plugins.sh plugin1 plugin2 plugin3

# Basculez vers l'utilisateur Jenkins pour exécuter Jenkins
USER jenkins

# Exposez le port de Jenkins (par défaut 8080)
EXPOSE 8080

# Démarrez Jenkins
CMD ["/usr/local/bin/jenkins.sh"]